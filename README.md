
### What is this repository for? ###

This repo is for the grasp learning and inference using the [Cornell grasp data-set](http://pr.cs.cornell.edu/grasping/rect_data/data.php). 

### Tensorflow version ###

The learning and inference were first developed with Tensorflow r0.
Then the learning is transferred to Tensorflow r1.1 using [tf_upgrade.py](https://www.tensorflow.org/install/migration).
The learning file for Tensorflow r1.1 is grasp_rgbd_net_tf_1.py. 