#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>

#include<fstream>

//int user_data;
//Example to use:
//./normal_est /media/kai/tars_HDD/cornell_grasp_data/01/pcd0100.txt tmp.pcd
int 
main (int argc, char* argv[])
{
    std::cout << argv[1] << " " << argv[2] << std::endl;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    //pcl::io::loadPCDFile ("/media/kai/tars_HDD/cornell_grasp_data/01/pcd0100.txt", *cloud);
    pcl::io::loadPCDFile (argv[1], *cloud);

    //uint32_t rgb = *reinterpret_cast<int*>(&cloud->points.begin()->rgb);
    //std::cout << "rgb: " << rgb << std::endl;
    //uint8_t r = (rgb >> 16) & 0x0000ff;
    //uint8_t g = (rgb >> 8)  & 0x0000ff;
    //uint8_t b = (rgb)       & 0x0000ff;
    //std::cout << "r: " << (int)r << "g: " << (int)g << "b: " << (int)b << std::endl; 
    //uint32_t label  = *reinterpret_cast<int*>(&cloud->points.begin()->label);
    //std::cout << "label: " << label << std::endl;

	// Create the normal estimation class, and pass the input dataset to it
	pcl::NormalEstimationOMP<pcl::PointXYZRGB, pcl::Normal> ne;
	ne.setInputCloud (cloud);

	// Create an empty kdtree representation, and pass it to the normal estimation object.
	// Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB> ());
	ne.setSearchMethod (tree);

	// Output datasets
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

	//ne.setRadiusSearch (30);
    int normals_k_neighbors = 50;
    ne.setKSearch(normals_k_neighbors);

	// Compute the features
	ne.compute (*cloud_normals);
    //std::cout << "Normal computed!" << cloud_normals->size() << std::endl;
	//pcl::io::savePCDFile ("normls.pcd", *cloud_normals);
    pcl::io::savePCDFile (argv[2], *cloud_normals);

   // boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
   // viewer->setBackgroundColor (0, 0, 0);
   // pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
   // viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
   // viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
   // viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal> (cloud, cloud_normals, 10, 10., "normals");
   // viewer->addCoordinateSystem (1.0);
   // viewer->initCameraParameters ();
   // 
   // while (!viewer->wasStopped ())
   // {
   //     viewer->spinOnce (100);
   //     user_data++;
   // }
    return 0;
}
